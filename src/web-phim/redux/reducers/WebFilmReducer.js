import { data } from "../../data";

const initialState = {
  seats: data,
  seatChoose: [],
};
export let webFilmReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "ADD":
      let index_hang = state.seats.findIndex((item) => {
        return item.hang == payload.slice(0, 1);
      });
      let index_soGhe = state.seats[index_hang].danhSachGhe.findIndex(
        (item) => {
          return item.soGhe == payload;
        }
      );
      let clone = [...state.seats];
      clone[index_hang].danhSachGhe[index_soGhe].daDat == true
        ? (clone[index_hang].danhSachGhe[index_soGhe].daDat = false)
        : (clone[index_hang].danhSachGhe[index_soGhe].daDat = true);

      return { ...state, seats: [...clone] };
    case "PAY":
      payload.forEach((item) => {
        state.seats.forEach((checkSeats, indexHang) => {
          checkSeats.danhSachGhe.forEach((check, indexGhe) => {
            if (check.soGhe == item) {
              state.seats[indexHang].danhSachGhe[indexGhe].paid = true;
            }
          });
        });
      });
      return { ...state, seats: [...state.seats] };
    default:
      return state;
  }
};
