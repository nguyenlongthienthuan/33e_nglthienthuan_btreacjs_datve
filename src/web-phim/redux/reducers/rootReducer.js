import { combineReducers } from "redux";
import { webFilmReducer } from "./WebFilmReducer";

export const rootReducer = combineReducers({
  webFilmReducer,
});
