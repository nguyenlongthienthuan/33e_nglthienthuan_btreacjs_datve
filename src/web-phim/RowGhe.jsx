import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemGhe from './ItemGhe'
import Total from './Total'

 class RowGhe extends Component {
  renderList=()=>{
    return this.props.dataSeat.map((item)=>{return <ItemGhe hang={item.hang} danhSachGhe={item.danhSachGhe}></ItemGhe>})
  }
  render() {
    return (
      <div className='container xl mx-auto bg-white/50 h-full '>
        <div><h2>Fill The Required Details Below And Select Your Seats</h2></div>
        <div><h2>Number of Seats  <input type="text" /></h2>
        <table className='listSeats w-1/2 mx-auto'>
          {this.renderList()}
      </table>
         <Total></Total>
          
        </div>
      </div>
    )
  }
}
const mapStateToProps=(state)=>{
   return{
    dataSeat:state.webFilmReducer.seats,
   }
}
const mapDispatchToProps=(dispatch)=>{
  return {
    paid:(payload)=>{
      return dispatch({
        type:"PAY",
        payload,
      })
    }
  }
}
export default connect(mapStateToProps)(RowGhe)