import React, { Component } from 'react'
import { connect } from 'react-redux'
import { handelChoose } from './redux/actions/chooseSeat'

 class ItemGhe extends Component {
    renderList=()=>{
         return this.props.Data.map((item,index)=>{
          return index==0? (
            <tr className='rowNumber'>
             <td>{item.hang}</td>
              {this.renderSoLuongGhe(item)}
             </tr>
          ):
          (
            <tr className='rowNumber'>
             <td>{item.hang}</td>
             {this.renderSoGhe(item)}
             </tr>
          )
        }
         )
    }
    renderSoLuongGhe=(data)=>{
        return data.map((item)=>{
            return <td>{item.soGhe}</td>
        })
    }
    chooseGhe=(soHang,soGhe)=>{
        let id='#'+soGhe;
       document.querySelector(id).classList.toggle('gheDuocChon');
    }
    renderSoGhe=(data)=>{
        return data.map((item,index)=>{
          let choose=item.daDat==true ? ' gheDuocChon':"";
          let className='ghe'+ choose;
          let paid=className+ (item.paid==true?' paid':"");
          return(
          <td onClick={()=>{this.props.handelChoose(item.soGhe)}}>
            <input className={paid} type="button" id={item.soGhe} />
          </td>
         )})
    }
  render() {
    let {hang,danhSachGhe}=this.props
    return (
      <tr className='rowNumber'>
      <td>{hang}</td>
       {hang!=''? this.renderSoGhe(danhSachGhe):this.renderSoLuongGhe(danhSachGhe)}
      </tr>
    )
  }
}
const mapStateToProps=(state)=>{
return{
   Data:state.webFilmReducer.seats,
}
}
const mapDispatchToProps=(dispatch)=>{
    return {
        handelChoose:(payload)=>{
            dispatch(handelChoose(payload)) 
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ItemGhe)