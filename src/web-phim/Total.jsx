import React, { Component } from 'react'
import { connect } from 'react-redux'
import { data } from './data';

class Total extends Component {
    checkSeat=()=>{
        let seats=[];
        let totalPrice=0;
        this.props.Data.forEach((item,index) => {
           if(index!=0){
              item.danhSachGhe.forEach((check) => {
                if(check.daDat==true){
                    seats.push(check.soGhe);
                    totalPrice+=check.gia;
                }
              });
           }
        });
        return {seats,totalPrice};
    }
  render() {
    return (
     <table className='table bg-white'>
        <thead>
            
               <tr> <td>
                seats
                </td>
                <td>
                Number of Seats
                </td>
                <td>
               prices
                </td></tr>
        </thead>
        <tbody>
            <tr>
                <td>{this.checkSeat().seats.length}</td>
                <td>{this.checkSeat().seats.map((item)=>{return item+','})}</td>
                <td>{this.checkSeat().totalPrice}</td>
            </tr>
        </tbody>
        <button onClick={()=>{this.props.paid(this.checkSeat().seats)}} className='btn btn-primary'>thanh toan</button>
     </table>
    )
  }
}
const mapStateToProps=(state)=>{
    return{
       Data:state.webFilmReducer.seats,
    }
    }
    const mapDispatchToProps=(dispatch)=>{
        return {
          paid:(payload)=>{
            return dispatch({
              type:"PAY",
              payload,
            })
          }
        }
      }
export default connect(mapStateToProps,mapDispatchToProps)(Total)